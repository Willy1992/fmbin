#include <string.h>
#include <stdlib.h>
#include "ctl_server.h"
#include "fmcommon.h"
#include "ipc/file.h"
#include "ipc/socket.h"

/*
 * This variable carries the context for the IPC implementations.
 * Ideally we wouldn't like this to be a static variable, but that requires
 * changing the signature of the send_interruption_info() function.
 * So, for the time being, let's keep it like this.
 */
static void *ipc_context = NULL;

/*
 * The selected implementation of the IPC gets decided in init_server() and is
 * stored in this static variable.
 */
static const ipc_functions_t *ipc = NULL;

const char MESSAGE_DELIMITER = 0x0c;

/**
 * Open server for receiving control requests
 * @param request_callback Callback - handler of each control request
 * @return
 */
int init_server(fm_srv_callback request_callback,
                const server_config_t *config) {
	ipc = config->request_file == NULL ?
		ipc_socket_functions() : ipc_file_functions();

	int ret = ipc->init(config, &ipc_context);
	if (ret < 0) {
		return ret;
	}

	boolean working = TRUE;

	ssize_t cmd_len;
	size_t res_len;
	char buf[CS_BUF];

	const char* exit = "exit";

	while (working) {
		// Clear
		memset((char*) &buf, 0, sizeof(buf));

		cmd_len = ipc->read_request(ipc_context, buf, sizeof(buf));

		if (cmd_len < 0) {
			print_err("server          : recvfrom error");
			continue;
		}

		print_dbg("server          : client received `%s`", buf);

		if (strcmp(buf, exit) == 0) {
			working = FALSE;
			break;
		}

		response_t res = request_callback(buf);
		res_len = strlen(res.data) * sizeof(char) + 1;

		// Send response after execute callback
		//rcv_len =
		ipc->send_response(ipc_context, res.data, res_len);

		// print_dbg("sent %zd bytes.", rcv_len);

		//free(&res.data);
	}

	ipc->close(ipc_context);
	return 0;
}

boolean send_interruption_info(int evt, char* message) {
	char buf[CS_BUF];

	// If message is NULL - replace it by empty string
	if (message == NULL) {
		message = "";
	}

	// Stringify message with format "${event_id}\x0c${message}"
	sprintf(buf, "%d%c%s", evt, MESSAGE_DELIMITER, message);

	int ret = ipc->send_interruption(ipc_context, buf, strlen(buf));

	return ret == 0 ? TRUE : FALSE;
}
