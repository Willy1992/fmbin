#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "fmcommon.h"
#include "ipc/socket.h"

#define CS_PORT 2112
#define CS_PORT_SRV 2113

typedef struct {
	int fd;
	struct sockaddr_in cli_addr;
	socklen_t cli_len;
} socket_context_t;

static int socket_init(const server_config_t *config, void **ctx)
{
	print_dbg("server          : starting server...");

	socket_context_t *c = malloc(sizeof(socket_context_t));
	if (!c) {
		print_err("Failed to allocate context memory");
		return -3;
	}

	memset(c, 0, sizeof(socket_context_t));
	*ctx = c; // will be passed to other socket functions

	// Create socket
	c->fd = socket(AF_INET, SOCK_DGRAM, 0);

	// Если не удалось слушать порт (например, он уже занят)
	if (c->fd < 0) {
		print_err("server          : socket init failed");
		return -1;
	}

	struct sockaddr_in srv_addr = {0};

	socklen_t srv_len = sizeof(struct sockaddr_in);

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	srv_addr.sin_port = htons(CS_PORT);

	// Bind socket to port
	if (bind(c->fd, (struct sockaddr*) &srv_addr, srv_len) < 0) {
		print_err("server          : bind error");
		return -2;
	}

	print_dbg("server          : started");
	return 0;
}

static ssize_t socket_read_request(void *ctx, void *buffer, size_t count)
{
	socket_context_t *c = ctx;

	// Clear
	memset(&c->cli_addr, 0, sizeof(struct sockaddr_in));

	c->cli_len = sizeof(struct sockaddr_in);

	return recvfrom(c->fd, buffer, count, 0,
	                (struct sockaddr *) &c->cli_addr, &c->cli_len);
}

static ssize_t socket_send_response(void *ctx, const void *buffer, size_t count)
{
	socket_context_t *c = ctx;
	return sendto(c->fd, buffer, count, 0,
	              (struct sockaddr*) &c->cli_addr, c->cli_len);
}

static ssize_t socket_send_interruption(void *ctx, const void *buffer, size_t count)
{
	int sock;
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(CS_PORT_SRV); // port
	addr.sin_addr.s_addr = INADDR_ANY; // address

	// Open connection
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		print_err("evt_send: socket < 0");
		return -1;
	}

	// Send data to server on application (Android Service)
	ssize_t ret = sendto(sock, buffer, strlen(buffer), MSG_CONFIRM,
	                     (struct sockaddr*) &addr, sizeof(addr));

	// Close connection
	close(sock);

	return ret;
}

static int socket_close(void *ctx)
{
	socket_context_t *c = ctx;
	// Close socket
	close(c->fd);
	free(c);
	print_dbg("server          : closed");
	return 0;
}

static const ipc_functions_t socket_functions = {
	.init = socket_init,
	.read_request = socket_read_request,
	.send_response = socket_send_response,
	.send_interruption = socket_send_interruption,
	.close = socket_close,
};

const ipc_functions_t *ipc_socket_functions() {
    return &socket_functions;
}
