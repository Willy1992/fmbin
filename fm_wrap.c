#include "fm_wrap.h"
#include <linux/videodev2.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <utils.h>
#include "ctl_server.h"
#include "utils.h"
#include "fm_ctl.h"
#include "fmcommon.h"
#include "detector.h"

// In case, if not defined
#ifndef V4L2_CID_PRIVATE_BASE
#    define V4L2_CID_PRIVATE_BASE                  0x8000000
#endif

enum tavarua_evt_t {
    TAVARUA_EVT_RADIO_READY = 0,
    TAVARUA_EVT_TUNE_SUCC,
    TAVARUA_EVT_SEEK_COMPLETE,
    TAVARUA_EVT_SCAN_NEXT,
    TAVARUA_EVT_NEW_RAW_RDS,
    TAVARUA_EVT_NEW_RT_RDS,
    TAVARUA_EVT_NEW_PS_RDS,
    TAVARUA_EVT_ERROR,
    TAVARUA_EVT_BELOW_TH,
    TAVARUA_EVT_ABOVE_TH,
    TAVARUA_EVT_STEREO,
    TAVARUA_EVT_MONO,
    TAVARUA_EVT_RDS_AVAIL,
    TAVARUA_EVT_RDS_NOT_AVAIL,
    TAVARUA_EVT_NEW_SRCH_LIST,
    TAVARUA_EVT_NEW_AF_LIST,
    TAVARUA_EVT_RADIO_DISABLED = 18,
    TAVARUA_EVT_NEW_RT_PLUS = 20,
    TAVARUA_EVT_NEW_ERT,
};

static char *qsoc_poweron_path = NULL;

volatile boolean is_power_on_completed = FALSE;

#define CHECK_EXEC_LAST_COMMAND(X,Y) if (ret == FALSE) {\
    print_err("%.*s: failed to set %s, exit code = %d", 16, X, Y, ret);\
    return FM_CMD_FAILURE;\
}

// FM asynchronous thread to perform the long running ON
pthread_t fm_interrupt_thread;
pthread_t fm_rssi_thread;

/**
 * Current state
 */
fm_current_storage fm_storage = {
        .frequency = 0,
        .band_type = FM_RX_US_EUROPE,
        .mute_mode = FM_RX_NO_MUTE,
        .space_type = FM_RX_SPACE_100KHZ,
        .stereo_type = FM_RX_STEREO,
        .state = OFF,
        .rssi = 0,
        .rds = {
                .radio_text = "",
                .program_name = "",
                .program_id = 0,
                .program_type = 0,
        },
};


/**
 * Process the radio event read from the V4L2 and perform the action by Radio event
 * Updates the global state: frequency, station available, RDS, etc.
 */
boolean process_radio_event(uint8 event_buf) {
    boolean ret = TRUE;

    switch (event_buf) {
        case TAVARUA_EVT_RADIO_READY: {
            print_dbg("fw_proc_event   : FM enabled");
            fm_storage.state = RX;
            send_interruption_info(EVT_ENABLED, "enabled");
            break;
        }

        case TAVARUA_EVT_RADIO_DISABLED: {
            print_dbg("fw_proc_event   : FM disabled");
            fm_storage.state = OFF;
            fm_receiver_close();
            pthread_exit(NULL);
            ret = FALSE;
            break;
        }

        case TAVARUA_EVT_TUNE_SUCC: {
            // Update current frequency
            fm_storage.frequency = fm_receiver_get_tuned_frequency();

            // Remove last RDS data
            strcpy(fm_storage.rds.program_name, "");
            strcpy(fm_storage.rds.radio_text, "");

            fm_storage.rssi = 0;

            print_dbg("fw_proc_event   : tuned to frequency %d", fm_storage.frequency);

            // Notify client about tune
            send_interruption_info(EVT_FREQUENCY_SET, int_to_string(fm_storage.frequency));
            send_interruption_info(EVT_UPDATE_PS, "");
            send_interruption_info(EVT_UPDATE_RT, "");
            send_interruption_info(EVT_UPDATE_PTY, "");
            send_interruption_info(EVT_UPDATE_PI, "");
            break;
        }

        case TAVARUA_EVT_SEEK_COMPLETE: {
            fm_storage.frequency = fm_receiver_get_tuned_frequency();

            print_dbg("fw_proc_event   : seek complete to frequency %d", fm_storage.frequency);
            send_interruption_info(EVT_SEEK_COMPLETE, int_to_string(fm_storage.frequency));
            break;
        }

        case TAVARUA_EVT_SCAN_NEXT: {
            print_dbg("fw_proc_event   : event scan next, frequency %d", fm_receiver_get_tuned_frequency());
            break;
        }

        case TAVARUA_EVT_NEW_RAW_RDS:
            print_dbg("fw_proc_event   : new RAW RDS");
            // extract_rds_af_list();
            break;

        case TAVARUA_EVT_NEW_RT_RDS: {
            ret = extract_radio_text(&fm_storage.rds);
            send_interruption_info(EVT_UPDATE_RT, fm_storage.rds.radio_text);
            break;
        }

        case TAVARUA_EVT_NEW_PS_RDS: {
            ret = extract_program_service(&fm_storage.rds);
            send_interruption_info(EVT_UPDATE_PS, fm_storage.rds.program_name);
            send_interruption_info(EVT_UPDATE_PI, int_to_hex_string(fm_storage.rds.program_id));
            send_interruption_info(EVT_UPDATE_PTY, int_to_string(fm_storage.rds.program_type));
            break;
        }

        case TAVARUA_EVT_ERROR: {
            print_err("fw_proc_event   : received error");
            break;
        }

        case TAVARUA_EVT_BELOW_TH: {
            print_dbg("fw_proc_event   : event below th");
            fm_storage.avail = FM_SERVICE_NOT_AVAILABLE;
            break;
        }

        case TAVARUA_EVT_ABOVE_TH: {
            print_dbg("fw_proc_event   : event above th");
            fm_storage.avail = FM_SERVICE_AVAILABLE;
            break;
        }

        case TAVARUA_EVT_STEREO: {
            print_dbg("fw_proc_event   : stereo mode");
            fm_storage.stereo_type = FM_RX_STEREO;
            send_interruption_info(EVT_STEREO, "1");
            break;
        }

        case TAVARUA_EVT_MONO: {
            print_dbg("fw_proc_event   : mono mode");
            fm_storage.stereo_type = FM_RX_MONO;
            send_interruption_info(EVT_STEREO, "0");
            break;
        }

        case TAVARUA_EVT_RDS_AVAIL: {
            print_dbg("fw_proc_event   : RDS available");
            // fm_global_params.rds_sync_status = FM_RDS_SYNCED;
            break;
        }

        case TAVARUA_EVT_RDS_NOT_AVAIL: {
            print_dbg("fw_proc_event   : RDS not available");
            // fm_global_params.rds_sync_status = FM_RDS_NOT_SYNCED;
            break;
        }

        case TAVARUA_EVT_NEW_SRCH_LIST: {
            print_dbg("fw_proc_event   : received new search list");
            uint32 list[25];
            uint8 stations = extract_search_station_list(list);

            char str[5 * stations];
            char *ptr = str;

            for (int i = 0; i < stations; ++i) {
                ptr += sprintf(ptr, "%04d", list[i] / 100);
            }

            print_dbg("SEARCH LIST = %s", str);

            send_interruption_info(EVT_SEARCH_DONE, str);
            break;
        }

        case TAVARUA_EVT_NEW_AF_LIST: {
            uint32 list[25];
            uint8 size = extract_rds_af_list(list);

            char* str = (char*) malloc(sizeof(char) * 5 * size);
            str[0] = '\0';
            char *ptr = str;

            for (int i = 0; i < size; ++i) {
                print_dbg("%d %d", list[i], list[i] / 100);
                ptr += sprintf(ptr, "%04d", list[i] / 100);
            }

            print_dbg("AF LIST = %s", str);

            send_interruption_info(EVT_UPDATE_AF, str);

            free(str);
            break;
        }

        default: {
            print_dbg("Unknown event RDS: %d", event_buf);
        }
    }
    /**
     * This logic is applied to ensure the exit of the Event read thread
     * before the FM Radio control is turned off. This is a temporary fix
     */
    return ret;
}

/**
 * Thread to perform a continuous read on the radio handle for events
 */
void* interrupt_thread(__attribute__((unused)) void* ignore) {
    print_dbg("fw_continue_th  : continue thread started");

    // Temporary buffer
    uint8 buf[128] = {0};

    // Status of process event
    boolean status;

    // Index for loop
    int i;

    // Count of bytes,
    uint32 bytes;

    while (1) {
        // Wait and read events
        bytes = read_data_from_v4l2(buf, TAVARUA_BUF_EVENTS);

        // If error occurred
        if (bytes < 0) {
            break;
        }

        // Process events
        for (i = 0; i < bytes; i++) {
            status = process_radio_event(buf[i]);
            if (status != TRUE) {
                goto exit;
            }
        }
    }
exit:

    print_dbg("fw_continue_th  : continue thread exited");
    return NULL;
}

/**
 * Get signal strength
 * In frontend need compute:
 *   Weakest strength = 139 = -116dB
 *   Strongest strength = 220 = -35dB
 *   To get dB need: -255 + N = -X (dB)
 */
/**
 * Thread for polling signal strength
 */
void *fm_thread_rssi(__attribute__((unused)) void *ptr) {
    print_dbg("fw_rssi_th      : start RSSI thread");

    int errors = 0;

    while (errors < 100) {
        wait(1000);
        if (send_interruption_info(EVT_UPDATE_RSSI, int_to_string(fm_receiver_get_rssi())) != TRUE) {
            ++errors;
        }
    }

    print_dbg("fw_rssi_th      : RSSI thread exited");
    return NULL;
}

/**
 * Part 1. Open file descriptor of radio
 * Opens the handle to /dev/radio0 V4L2 device.
 * @return FM command status
 */
fm_cmd_status_t fm_command_open() {
    int exit_code = system("setprop hw.fm.mode normal >/dev/null 2>/dev/null; setprop vendor.hw.fm.mode normal >/dev/null 2>/dev/null; setprop hw.fm.version 0 >/dev/null 2>/dev/null; setprop vendor.hw.fm.version 0 >/dev/null 2>/dev/null; setprop ctl.start fm_dl >/dev/null 2>/dev/null");
    (void)exit_code;  // Avoid an "unused variable" warning when DEBUG is disabled
    print_dbg("fw_cmd_open     : setprop exit code %d", exit_code);

    if (file_exists("/system/lib/modules/radio-iris-transport.ko")) {
        print_dbg("fw_cmd_open     : found radio-iris-transport.ko, insmod it");
        system("insmod /system/lib/modules/radio-iris-transport.ko >/dev/null 2>/dev/null");
    }

    char value[4] = {0x41, 0x42, 0x43, 0x44};

    uint16 attempt;
    int init_success = 0;

    for (attempt = 0; attempt < 600; ++attempt) {
        get_system_prop("hw.fm.init", value, sizeof(value));
        if (value[0] == '1') {
            init_success = 1;
            break;
        }
        get_system_prop("vendor.hw.fm.init", value, sizeof(value));
        if (value[0] == '1') {
            init_success = 1;
            break;
        }
        wait(10);
    }

    if (init_success) {
        print_dbg("fw_cmd_open     : init success after %d attempts", attempt + 1);
    } else {
        print_err("fw_cmd_open     : init failed after %d attempts, exiting...", attempt);
        return FM_CMD_FAILURE;
    }

    wait(500);

    print_dbg("fw_cmd_open     : open /dev/radio0...");

    boolean ret = fm_receiver_open();

    if (ret == FALSE) {
        print_err("fw_cmd_open    : failed to open fd_radio");
        return FM_CMD_FAILURE;
    }

    wait(700);

    return FM_CMD_SUCCESS;
}


/**
 * Part 2. Check version of driver, set version in system_properties
 * Initiates a soc patch download.
 */
fm_cmd_status_t fm_command_prepare(fm_config_data *config_ptr) {
    uint32 ret;
    char version_str[40] = {'\0'};
    struct v4l2_capability cap = {};

    print_dbg("fw_cmd_prepare  : read the driver versions...");

    // Read the driver version
    ret = fm_receiver_query_capabilities(&cap);

    print_dbg("fw_cmd_prepare  : VIDIOC_QUERYCAP returns: ret=%d; version=0x%x", ret, cap.version);

    if (ret == TRUE) {
        print_dbg("fw_cmd_prepare  : driver version (same as chip id): 0x%x", cap.version);

        // Convert the integer to string
        ret = snprintf(version_str, sizeof(version_str), "%d", cap.version);

        if (ret >= sizeof(version_str)) {
            print_err("fw_cmd_prepare  : version check failed");
            fm_receiver_close();
            return FM_CMD_FAILURE;
        }

        set_system_prop("hw.fm.version", version_str);
        set_system_prop("vendor.hw.fm.version", version_str);

        print_dbg("fw_cmd_prepare  : hw.fm.version = %s", version_str);

        asprintf(&qsoc_poweron_path, "fm_qsoc_patches %d 0", cap.version);

        if (qsoc_poweron_path != NULL) {
            print_dbg("fw_cmd_prepare  : qsoc_onpath = %s", qsoc_poweron_path);
        }
    } else {
        print_err("fw_cmd_prepare  : ioctl failed");
        return FM_CMD_FAILURE;
    }

    print_dbg("fw_cmd_prepare  : opened receiver successfully");
    return fm_command_setup_receiver(config_ptr);
}

/**
 * Part 3. Setup receiver
 * Configure initial parameters like band limit, RDS system, frequency band and radio state
 */
fm_cmd_status_t fm_command_setup_receiver(fm_config_data *ptr) {
    int ret;

    fm_config_data* cfg = (fm_config_data*) ptr;

#ifdef __ANDROID_API__
    if (is_smd_transport_layer()) {
        system("setprop ctl.start fm_dl >/dev/null 2>/dev/null");
        sleep(1);
    } else if (!is_rome_chip()) {
        ret = system(qsoc_poweron_path);
        if (ret != 0) {
            print_err("fw_setup_recei  : failed to download patches = %d", ret);
            return FM_CMD_FAILURE;
        }
    }
#endif

    /**
     * V4L2_CID_PRIVATE_TAVARUA_STATE
     * V4L2_CID_PRIVATE_TAVARUA_EMPHASIS
     * V4L2_CID_PRIVATE_TAVARUA_SPACING
     * V4L2_CID_PRIVATE_TAVARUA_RDS_STD
     * V4L2_CID_PRIVATE_TAVARUA_REGION
     */

    // Enable RX (Receiver)
    ret = fm_receiver_set_state(RX);

    // If cannot set state, finish
    if (ret == FALSE) {
        print_err("fw_setup_recei  : failed to set radio state = %d", ret);
        fm_receiver_close();
        return FM_CMD_FAILURE;
    }

    // Power level
    // set_v4l2_ctrl(fd_radio, V4L2_CID_TUNE_POWER_LEVEL, 7);

    // Emphasis (50/75 kHz)
    print_dbg("fw_setup_recei  : emphasis = %d", cfg->emphasis);
    ret = fm_receiver_set_emphasis(cfg->emphasis);
    CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change emphasis");

    // Spacing (50/100/200kHz)
    print_dbg("fw_setup_recei  : spacing = %d", cfg->spacing);
    ret = fm_receiver_set_spacing(cfg->spacing);
    CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change channel spacing");

    // Set band and range frequencies
    //ret = fm_receiver_set_band(cfg->band);
    //CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change band and limit frequencies");


    // Set antenna
    ret = fm_receiver_set_antenna(0);
    CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change antenna");

    // Create threads
    pthread_create(&fm_interrupt_thread, NULL, interrupt_thread, NULL);
    pthread_create(&fm_rssi_thread, NULL, fm_thread_rssi, NULL);

    is_power_on_completed = TRUE;
    return FM_CMD_SUCCESS;
}

fm_cmd_status_t fm_command_setup_rds(rds_system_t system) {
    int ret;

    // RDS enable
    ret = fm_receiver_set_rds_state(system != FM_RX_NO_RDS_SYSTEM);
    CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "enable RDS");

    //
    ret = fm_receiver_set_rds_system(system);
    CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change RDS system");

    // RDS system standard
    print_dbg("fw_cmd_set_rds  : RDS system = %d", system);

    // If RDS enabled
    if (system != FM_RX_NO_RDS_SYSTEM) {

        //int32 rds_mask = fm_receiver_get_rds_group_options();

        // TODO ????
        //uint8 rds_group_mask = ((rds_mask & 0xC7) & 0x07) << 3;
        // int psAllVal = ;

        //print_dbg("fw_cmd_set_rds  : rds_options: %x", rds_group_mask);

        uint32 mask = 0xffff;

        print_dbg("fw_cmd_set_rds  : set rds group options = 0x%x", mask);
        ret = fm_receiver_set_rds_group_options(/*rds_group_mask*/ mask); // 0xff OK
        CHECK_EXEC_LAST_COMMAND(__FUNCTION__, "change RDS group options");
    }

    if (is_rome_chip()) {
        print_dbg("fw_cmd_set_rds  : rds - is rome");
        /*ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_RDSGROUP_MASK, 1);
        if (ret == FALSE) {
            print_err("Failed to set RDS GRP MASK");
            return FM_CMD_FAILURE;
        }
        ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_RDSD_BUF, 1);
        if (ret == FALSE) {
            print_err("Failed to set RDS BUF");
            return FM_CMD_FAILURE;
        }*/
    } else {
        uint32 ps_all = 0xffff;
        print_dbg("fw_cmd_set_rds  : rds - isn't rome, set ps all = 0x%x", ps_all);
        ret = fm_receiver_set_ps_all(ps_all); // ???
        if (ret == FALSE) {
            print_err("fw_cmd_set_rds  : failed to set RDS ps all");
            return FM_CMD_FAILURE;
        }
    }

    return FM_CMD_SUCCESS;
}

/**
 * Part N. Disable radio
 * Close the handle to /dev/radio0 V4L2 device.
 */
fm_cmd_status_t fm_command_disable() {
    print_dbg("fw_cmd_disable  : call");

    // Wait till the previous ON sequence has completed
    if (is_power_on_completed != TRUE) {
        print_dbg("fw_cmd_disable  : already disabled");
        return FM_CMD_FAILURE;
    }

    print_dbg("fw_cmd_disable  : set state = 0...");

    boolean ret = fm_receiver_set_state(OFF);

    if (ret == FALSE) {
        print_err("fw_cmd_disable  : failed to set fm off");
        return FM_CMD_FAILURE;
    }

    system("setprop ctl.stop fm_dl >/dev/null 2>/dev/null");

    print_dbg("fw_cmd_disable  : successfully");

    return FM_CMD_SUCCESS;
}

/**
 * Part 4.1. Set frequency (kHz).
 * Tune to specified frequency.
 */
fm_cmd_status_t fm_command_tune_frequency(uint32 frequency) {
    print_dbg("fw_cmd_set_freq : call with freq = %d", frequency);

    boolean ret = fm_receiver_set_tuned_frequency(frequency);

    if (ret == FALSE) {
        print_err("fw_cmd_set_freq : failed");
        return FM_CMD_FAILURE;
    }

    print_dbg("fw_cmd_set_freq : successfully");

    return FM_CMD_SUCCESS;
}

/**
 * Part 4.2. Set frequency by delta (current = 104000 kHz, delta = 100 kHz, expect = 104100 kHz).
 * @param direction Direction of delta
 */
fm_cmd_status_t fm_command_tune_frequency_by_delta(signed short direction) {
    // Current
    uint32 current_frequency_khz = fm_receiver_get_tuned_frequency();

    int32 delta;

    switch (fm_storage.space_type) {
        case FM_RX_SPACE_200KHZ: delta = 200; break;
        case FM_RX_SPACE_100KHZ: delta = 100; break;
        case FM_RX_SPACE_50KHZ: delta = 50; break;
    }

    // Required
    uint32 need_frequency_khz = current_frequency_khz + (delta * direction);

    // Change
    return fm_receiver_set_tuned_frequency(need_frequency_khz);
}

/**
 * Part 4.3. Returns frequency in kHz.
 */
uint32 fm_command_get_tuned_frequency() {
    return fm_receiver_get_tuned_frequency();
}


/**
 * Part 5. Configure mute mode
 * @return FM command status
 */
fm_cmd_status_t fm_command_set_mute_mode(mute_t mode) {
    int ret = fm_receiver_set_mute_mode(mode);

    if (ret == TRUE) {
        print_dbg("fw_cmd_set_mute : successfully");
        return FM_CMD_SUCCESS;
    }

    return FM_CMD_FAILURE;
}

fm_cmd_status_t fm_command_set_stereo_mode(stereo_t is_stereo) {
    return fm_receiver_set_stereo_mode(is_stereo);
}


/**
 * fm_receiver_set_rds_options
 * PFAL specific routine to configure the FM receiver's RDS options
 * @return FM command status
 */
/*fm_cmd_status_t fm_receiver_set_rds_options(fm_rds_options options) {
    int ret;
    print_dbg("fm_receiver_set_rds_options");

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_RDSGROUP_MASK, options.rds_group_mask);

    if (ret == FALSE) {
        print_err("fm_receiver_set_rds_options Failed to set RDS group options = %d", ret);
        return FM_CMD_FAILURE;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_RDSD_BUF, options.rds_group_buffer_size);

    if (ret == FALSE) {
        print_err("fm_receiver_set_rds_options Failed to set RDS group options = %d", ret);
        return FM_CMD_FAILURE;
    }

    // Change Filter not supported
    print_dbg("fm_receiver_set_rds_options<");

    return FM_CMD_SUCCESS;
}


/ **
 * fm_receiver_set_signal_threshold
 * PFAL specific routine to configure the signal threshold of FM receiver
 * @return FM command status
 * /
fm_cmd_status_t fm_receiver_set_signal_threshold(uint8 threshold) {
    struct v4l2_control control;
    int i, err;
    print_dbg("fm_receiver_set_signal_threshold threshold = %d", threshold);
    if (fd_radio < 0) {
        return FM_CMD_NO_RESOURCES;
    }

    control.value = threshold;
    control.id = V4L2_CID_PRIVATE_TAVARUA_SIGNAL_TH;

    for (i = 0; i < 3; i++) {
        err = ioctl(fd_radio, VIDIOC_S_CTRL, &control);
        if (err >= 0) {
            print_dbg("fm_receiver_set_signal_threshold Success");
            return FM_CMD_SUCCESS;
        }
    }
    return FM_CMD_SUCCESS;
}

/ **
 * fm_receiver_search_stations
 * PFAL specific routine to search for stations from the current frequency of
 * FM receiver and print the information on diag
 * @return FM command status
 * /
fm_cmd_status_t fm_receiver_search_stations(fm_search_stations options) {
    int err, i;
    struct v4l2_control control;
    struct v4l2_hw_freq_seek hwseek;
    boolean ret;

    hwseek.type = V4L2_TUNER_RADIO;
    print_dbg("fm_receiver_search_stations");

    if (fd_radio < 0) {
        return FM_CMD_NO_RESOURCES;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SRCHMODE, options.search_mode);
    if (ret == FALSE) {
        print_err("fm_receiver_search_stations failed");
        return FM_CMD_FAILURE;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SCANDWELL, options.dwell_period);
    if (ret == FALSE) {
        print_err("fm_receiver_search_stations failed");
        return FM_CMD_FAILURE;
    }

    hwseek.seek_upward = options.search_dir;
    err = ioctl(fd_radio, VIDIOC_S_HW_FREQ_SEEK, &hwseek);

    if (err < 0) {
        print_err("fm_receiver_search_stations failed");
        return FM_CMD_FAILURE;
    }
    print_dbg("fm_receiver_search_stations<");
    return FM_CMD_SUCCESS;
}


/ **
 * PFAL specific routine to search for stations from the current frequency of
 * FM receiver with a specific program type and print the information on diag
 * @return FM command status
 * /
fm_cmd_status_t fm_receiver_search_rds_stations(fm_search_rds_stations options) {
    int i, err;
    boolean ret;
    struct v4l2_control control;
    struct v4l2_hw_freq_seek hwseek;

    hwseek.type = V4L2_TUNER_RADIO;
    print_dbg("fm_receiver_search_rds_stations>");

    if (fd_radio < 0) {
        return FM_CMD_NO_RESOURCES;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SRCHMODE, options.search_mode);
    if (ret == FALSE) {
        print_err("fm_receiver_search_rds_stations failed");
        return FM_CMD_FAILURE;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SCANDWELL, options.dwell_period);
    if (ret == FALSE) {
        print_err("fm_receiver_search_rds_stations failed");
        return FM_CMD_FAILURE;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SRCH_PTY, options.program_type);
    if (ret == FALSE) {
        print_err("fm_receiver_search_rds_stations failed");
        return FM_CMD_FAILURE;
    }

    ret = set_v4l2_ctrl(fd_radio, V4L2_CID_PRIVATE_TAVARUA_SRCH_PI, options.program_id);
    if (ret == FALSE) {
        print_err("fm_receiver_search_rds_stations failed");
        return FM_CMD_FAILURE;
    }

    hwseek.seek_upward = options.search_dir;
    err = ioctl(fd_radio, VIDIOC_S_HW_FREQ_SEEK, &hwseek);

    if (err < 0) {
        print_err("fm_receiver_search_rds_stations failed");
        return FM_CMD_FAILURE;
    }

    print_dbg("fm_receiver_search_rds_stations<");
    return FM_CMD_SUCCESS;
}
*/
