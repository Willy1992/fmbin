#include <string.h>
#include "detector.h"
#include "fmcommon.h"
#include "utils.h"

boolean is_smd_transport_layer() {
    char buf[40];

    get_system_prop("ro.qualcomm.bt.hci_transport", buf, sizeof(buf));

    return strcmp(buf, "smd") == 0;
}

boolean is_rome_chip() {
    char buf[40];

    get_system_prop("vendor.bluetooth.soc", buf, sizeof(buf));

    return strcmp(buf, "rome") == 0;
}

boolean is_cherokee_chip() {
    char buf[40];

    get_system_prop("vendor.bluetooth.soc", buf, sizeof(buf));

    return strcmp(buf, "cherokee") == 0;
}
